/* tslint:disable */

declare var Object: any;
export interface RecoverylistInterface {
  "id": any;
  "eventid": any;
  "accept": any;
  "finishDate": any;
  "ifArrivalid": any;
  "diagnosis": any;
  "reasonid": any;
  "actionsid": any;
  "moduleid": any;
  "comment": any;
  "ifFinishid": any;
}

export class Recoverylist implements RecoverylistInterface {
  "id": any;
  "eventid": any;
  "accept": any;
  "finishDate": any;
  "ifArrivalid": any;
  "diagnosis": any;
  "reasonid": any;
  "actionsid": any;
  "moduleid": any;
  "comment": any;
  "ifFinishid": any;
  constructor(data?: RecoverylistInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Recoverylist`.
   */
  public static getModelName() {
    return "Recoverylist";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Recoverylist for dynamic purposes.
  **/
  public static factory(data: RecoverylistInterface): Recoverylist{
    return new Recoverylist(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Recoverylist',
      plural: 'Recoverylists',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "eventid": {
          name: 'eventid',
          type: 'any'
        },
        "accept": {
          name: 'accept',
          type: 'any'
        },
        "finishDate": {
          name: 'finishDate',
          type: 'any'
        },
        "ifArrivalid": {
          name: 'ifArrivalid',
          type: 'any'
        },
        "diagnosis": {
          name: 'diagnosis',
          type: 'any'
        },
        "reasonid": {
          name: 'reasonid',
          type: 'any'
        },
        "actionsid": {
          name: 'actionsid',
          type: 'any'
        },
        "moduleid": {
          name: 'moduleid',
          type: 'any'
        },
        "comment": {
          name: 'comment',
          type: 'any'
        },
        "ifFinishid": {
          name: 'ifFinishid',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
