/* tslint:disable */
import {
  Events
} from '../index';

declare var Object: any;
export interface GeoInterface {
  "id": any;
  "event_id": any;
  "mechanic_id": any;
  "lat": any;
  "lng": any;
  "date": any;
  "device": any;
  "id_when": any;
  Event?: Events[];
  Mechanic?: Events[];
}

export class Geo implements GeoInterface {
  "id": any;
  "event_id": any;
  "mechanic_id": any;
  "lat": any;
  "lng": any;
  "date": any;
  "device": any;
  "id_when": any;
  Event: Events[];
  Mechanic: Events[];
  constructor(data?: GeoInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Geo`.
   */
  public static getModelName() {
    return "Geo";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Geo for dynamic purposes.
  **/
  public static factory(data: GeoInterface): Geo{
    return new Geo(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Geo',
      plural: 'Geos',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "event_id": {
          name: 'event_id',
          type: 'any'
        },
        "mechanic_id": {
          name: 'mechanic_id',
          type: 'any'
        },
        "lat": {
          name: 'lat',
          type: 'any'
        },
        "lng": {
          name: 'lng',
          type: 'any'
        },
        "date": {
          name: 'date',
          type: 'any'
        },
        "device": {
          name: 'device',
          type: 'any'
        },
        "id_when": {
          name: 'id_when',
          type: 'any'
        },
      },
      relations: {
        Event: {
          name: 'Event',
          type: 'Events[]',
          model: 'Events'
        },
        Mechanic: {
          name: 'Mechanic',
          type: 'Events[]',
          model: 'Events'
        },
      }
    }
  }
}
