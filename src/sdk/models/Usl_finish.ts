/* tslint:disable */

declare var Object: any;
export interface Usl_finishInterface {
  "id": any;
  "usl": any;
}

export class Usl_finish implements Usl_finishInterface {
  "id": any;
  "usl": any;
  constructor(data?: Usl_finishInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Usl_finish`.
   */
  public static getModelName() {
    return "Usl_finish";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Usl_finish for dynamic purposes.
  **/
  public static factory(data: Usl_finishInterface): Usl_finish{
    return new Usl_finish(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Usl_finish',
      plural: 'Usl_finishes',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "usl": {
          name: 'usl',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
