import { Component,ViewChild } from '@angular/core';
import { NavController, NavParams,ModalController,AlertController } from 'ionic-angular';
import { LocationTracker } from '../../providers/location-tracker';
import { DopService } from '../../providers/dop-service';
import { AddReportPage } from '../addreport/addreport';
import { ContainerApi } from '../../sdk/services/index';
import { LoopBackConfig } from '../../sdk/lb.config';
import { Slides} from 'ionic-angular';

@Component({
  selector: 'page-alarm-details',
  templateUrl: 'alarm-details.html'
})
export class AlarmDetailsPage {
public Containers = [];
public urlpath: string;
public realIndex:number;
public ReportList = [];
public item: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public locationTracker: LocationTracker,
              public alertCtrl: AlertController,
              public modalCtrl: ModalController,
              public containerapi: ContainerApi,
              public dopService:DopService) {
  
  this.item = navParams.get('item');
  let self = this;
  self.containerapi.getFiles(self.item.id)
                                    .subscribe((files) => {
                                               self.Containers = files;
                                                           });

 
   self.urlpath= LoopBackConfig.getPath()  + "/" + LoopBackConfig.getApiVersion();
   //console.log("self.urlpath =>", self.urlpath);
    }
  start(){
      this.alertsignal('запуск трекинга');
   //   this.locationTracker.startTracking();
  }
  stop(){
      this.alertsignal('остановка трекинга');
      this.locationTracker.stopTracking();
  }
 
  //----------------------------------------------------------------------
  @ViewChild('mySlider') slider: Slides;

   addReport() {
    let self=this;  
    console.log('открываем отчет:'+self.item.id);  
   
    let addReportModal = self.modalCtrl.create(AddReportPage,{"id": self.item.id});
    addReportModal.present();
    addReportModal.onDidDismiss(data=>{ 
        self.ReportList=data;
           });
      }
  removePhoto() {
  let self = this;
  self.realIndex = this.slider.getActiveIndex();
  self.containerapi.removeFile(self.item.id,self.Containers[self.realIndex-1].name)
                                    .subscribe((files) => {
                                              self.Containers = files;
                                                             });

    }
    alertsignal(title:string) {
        let self = this;
        let alertBox = self.alertCtrl.create({
              title: title,
              buttons: [ {
                  text: 'Ок',
                  handler: () => {
                  
                                 }
                 }]
              });     
              alertBox.present();
    }
    startwork(){
     let res:any;
     res=this.dopService.takealarm(this.item.id);
     console.log('действо выполнено')
    }

 }
