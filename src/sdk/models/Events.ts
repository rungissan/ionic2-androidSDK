/* tslint:disable */
import {
  Lift,
  User,
  Contract
} from '../index';

declare var Object: any;
export interface EventsInterface {
  "id": any;
  "liftid": any;
  "contractid": any;
  "date": any;
  "description": any;
  "mechanicid": any;
  "status"?: any;
  "read_date"?: any;
  "report_date"?: any;
  "date_reportup"?: any;
  "mechanicId"?: any;
  lift?: Lift;
  mechanic?: User;
  user?: User;
  contract?: Contract;
}

export class Events implements EventsInterface {
  "id": any;
  "liftid": any;
  "contractid": any;
  "date": any;
  "description": any;
  "mechanicid": any;
  "status": any;
  "read_date": any;
  "report_date": any;
  "date_reportup": any;
  "mechanicId": any;
  lift: Lift;
  mechanic: User;
  user: User;
  contract: Contract;
  constructor(data?: EventsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Events`.
   */
  public static getModelName() {
    return "Events";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Events for dynamic purposes.
  **/
  public static factory(data: EventsInterface): Events{
    return new Events(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Events',
      plural: 'Events',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "liftid": {
          name: 'liftid',
          type: 'any'
        },
        "contractid": {
          name: 'contractid',
          type: 'any'
        },
        "date": {
          name: 'date',
          type: 'any'
        },
        "description": {
          name: 'description',
          type: 'any'
        },
        "mechanicid": {
          name: 'mechanicid',
          type: 'any'
        },
        "status": {
          name: 'status',
          type: 'any'
        },
        "read_date": {
          name: 'read_date',
          type: 'any'
        },
        "report_date": {
          name: 'report_date',
          type: 'any'
        },
        "date_reportup": {
          name: 'date_reportup',
          type: 'any'
        },
        "mechanicId": {
          name: 'mechanicId',
          type: 'any'
        },
      },
      relations: {
        lift: {
          name: 'lift',
          type: 'Lift',
          model: 'Lift'
        },
        mechanic: {
          name: 'mechanic',
          type: 'User',
          model: 'User'
        },
        user: {
          name: 'user',
          type: 'User',
          model: 'User'
        },
        contract: {
          name: 'contract',
          type: 'Contract',
          model: 'Contract'
        },
      }
    }
  }
}
