/* tslint:disable */
import {
  Reports
} from '../index';

declare var Object: any;
export interface Cause_alarmInterface {
  "id": any;
  "usl": any;
  Report?: Reports[];
}

export class Cause_alarm implements Cause_alarmInterface {
  "id": any;
  "usl": any;
  Report: Reports[];
  constructor(data?: Cause_alarmInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cause_alarm`.
   */
  public static getModelName() {
    return "Cause_alarm";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cause_alarm for dynamic purposes.
  **/
  public static factory(data: Cause_alarmInterface): Cause_alarm{
    return new Cause_alarm(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cause_alarm',
      plural: 'Cause_alarms',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "usl": {
          name: 'usl',
          type: 'any'
        },
      },
      relations: {
        Report: {
          name: 'Report',
          type: 'Reports[]',
          model: 'Reports'
        },
      }
    }
  }
}
