/* tslint:disable */
import {
  Reports
} from '../index';

declare var Object: any;
export interface Usl_arrivalInterface {
  "id": any;
  "usl": any;
  Report?: Reports[];
}

export class Usl_arrival implements Usl_arrivalInterface {
  "id": any;
  "usl": any;
  Report: Reports[];
  constructor(data?: Usl_arrivalInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Usl_arrival`.
   */
  public static getModelName() {
    return "Usl_arrival";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Usl_arrival for dynamic purposes.
  **/
  public static factory(data: Usl_arrivalInterface): Usl_arrival{
    return new Usl_arrival(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Usl_arrival',
      plural: 'Usl_arrivals',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "usl": {
          name: 'usl',
          type: 'any'
        },
      },
      relations: {
        Report: {
          name: 'Report',
          type: 'Reports[]',
          model: 'Reports'
        },
      }
    }
  }
}
