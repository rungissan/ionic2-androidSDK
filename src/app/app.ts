import {Component, ViewChild} from '@angular/core';
import {Platform,Nav,AlertController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Deeplinks} from '@ionic-native/deeplinks';
import {DetailsPage} from  '../pages/details/details';
import { AlarmsPage } from '../pages/alarms/alarms';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import {LoopBackAuth} from '../sdk/services/index';
import { BackgroundMode } from '@ionic-native/background-mode';
import { MediaPlugin,MediaObject } from '@ionic-native/media';
import { File } from '@ionic-native/file';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { LocationTracker } from '../providers/location-tracker';

declare let FCMPlugin: any;

@Component({
  template: '<ion-nav [root]="rootPage"></ion-nav>'
 })
export class ConnectKZ {
  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;
  accessTokenId: string;
  rootPage:any;
 
   constructor(public platform: Platform,
              protected auth: LoopBackAuth,
              public alertCtrl: AlertController,
              private backgroundMode: BackgroundMode,
              private media: MediaPlugin,
              private file: File,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private deeplinks: Deeplinks,
              private localNotifications: LocalNotifications,
              public locationTracker:LocationTracker) {
           
       this.initializeApp();
       this.accessTokenId=this.auth.getAccessTokenId();
          if (this.accessTokenId) {
                   this.rootPage=HomePage;
                     } else {
                             this.rootPage=LoginPage;
                             }
                     
    }
    
   initializeApp() {
  let self = this;
  /// Throw this in a provider constructor you will use for notifications:
this.platform.ready().then(() => {
   self.backgroundMode.enable();
   console.log("backgroundMode was enabled ----------");
   this.statusBar.styleDefault();
   this.splashScreen.hide();
//   self.locationTracker.putMyGeolocation(6);
  if(typeof(FCMPlugin) !== "undefined"){
    FCMPlugin.getToken(function(t){
      console.log("Use this token for sending device specific messages\nToken: " + t);
        }, function(e){
      console.log("Uh-Oh!\n"+e);
    });
  //  FCMPlugin.subscribeToTopic('foo-bar');
    FCMPlugin.onNotification(function(d){
          if (d.type==='2')  self.locationTracker.putMyGeolocation(parseInt(d.alarm),7);
        // Foreground receival, update UI or what have you...
         console.log("Foreground Foreground Foreground Foreground Foreground Foreground  Foreground ");
           if (self.backgroundMode.isActive()) {
                    console.log("backgroundMode was enabled 2----------");
                    self.backgroundMode.moveToForeground();
                       
            }
            
               // Schedule a single notification
             self.localNotifications.schedule({
                 id: 1,
                 text: d.line1,
                 sound: self.file.applicationDirectory+'www/assets/sounds/ne_dam.mp3',
                 data: { secret: 'key' }
               }); 

              this.alertBox = self.alertCtrl.create({
              title: d.title,
              subTitle: d.line1,
              buttons: [{
                text: 'Игнор',
                role: 'cancel',
                handler: () => {
                console.log('cancel');
                }
              }, {
                  text: 'Смотреть',
                  handler: () => {
                    self.nav.push(AlarmsPage);
                                 }
                 }]
              });     
              this.alertBox.present();
              let path = self.file.applicationDirectory+'www/assets/sounds/ne_dam.mp3';
              console.log("чудо путь:"+path);
              const onStatusUpdate = (status) => console.log(status);
              const onSuccess = () => console.log('Action is successful.');
              const onError = (error) => console.error(error.message);

             const fille: MediaObject = self.media.create(path, onStatusUpdate, onSuccess, onError);

              // play the file
              fille.play();

    }, function(msg){
       // No problemo, registered callback
    }, function(err){
      console.log("Arf, no good mate... " + err);
    });
  } else console.log("Notifications disabled, only provided in Android/iOS environment");
});
    
   }
   ngAfterViewInit() {
    this.platform.ready().then(() => {
       // Convenience to route with a given nav
      this.deeplinks.routeWithNavController(this.nav, {
        '/about-us': DetailsPage,
        '/universal-links-test': LoginPage,
        '/products/:productId': DetailsPage
      }).subscribe((match) => {
        console.log('Successfully routed', match);
      }, (nomatch) => {
        console.log('Unmatched Route', nomatch);
      });
    })
  }

}

