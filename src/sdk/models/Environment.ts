/* tslint:disable */
import {
  Lift
} from '../index';

declare var Object: any;
export interface EnvironmentInterface {
  "id": any;
  "type": any;
  Lift?: Lift[];
}

export class Environment implements EnvironmentInterface {
  "id": any;
  "type": any;
  Lift: Lift[];
  constructor(data?: EnvironmentInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Environment`.
   */
  public static getModelName() {
    return "Environment";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Environment for dynamic purposes.
  **/
  public static factory(data: EnvironmentInterface): Environment{
    return new Environment(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Environment',
      plural: 'Environments',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "type": {
          name: 'type',
          type: 'any'
        },
      },
      relations: {
        Lift: {
          name: 'Lift',
          type: 'Lift[]',
          model: 'Lift'
        },
      }
    }
  }
}
