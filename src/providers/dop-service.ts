import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { LocationTracker } from '../providers/location-tracker';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { GeoApi }  from '../sdk/services/index';
import {Observable} from 'rxjs/Rx';

import { LoopBackConfig } from '../sdk/lb.config';
interface Answer {
    status: string;
}


@Injectable()
export class DopService {
public watch:Geoposition;
public lat:number;
public lng:number;
public times:number;
public url = LoopBackConfig.getPath()  + "/" + LoopBackConfig.getApiVersion();
  

  constructor(public http: Http,
              public locationTracker:LocationTracker,
              public geolocation:Geolocation,
              private toastCtrl: ToastController,
              private geoApi:GeoApi) { }

  
   presentToast(message:string) {
   let toast =this.toastCtrl.create({
    message: message,
    duration: 3000,
    position: 'top'
  });
  toast.present();
}

takealarm (param:number) {
    let self=this;
    this.locationTracker.putMyGeolocation(param,3);
   let urlalarm =this.url+'/Events/get_event?msg='+param;
   // let urlalarm =this.url+'/Events/get_event?msg=49';
    console.log('Передача урл: '+urlalarm);
    this.getSignal(urlalarm).subscribe(
        res => { self.presentToast( res.status);}, //Bind to view
                                err => {
                                    // Log errors if any
                                    console.log(err);
                                }); 
}

   
     getSignal(urlparam:string) : Observable<Answer> {
         return this.http.get(urlparam)
                         .map((res:Response) => res.json())
                         .first()
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

     }            
}
