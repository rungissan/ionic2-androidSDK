/* tslint:disable */
import {
  User,
  Events,
  Route,
  Type,
  LiftType,
  Contract,
  Environment,
  Business,
  ServiceType
} from '../index';

declare var Object: any;
export interface LiftInterface {
  "elevatorn"?: any;
  "localn"?: any;
  "typeid"?: any;
  "lifttypeid"?: any;
  "address"?: any;
  "contractid"?: any;
  "servicetypeid"?: any;
  "busnissid"?: any;
  "serviceid"?: any;
  "mechanicid"?: any;
  "superviserid"?: any;
  "floors"?: any;
  "doors"?: any;
  "doorop"?: any;
  "envid"?: any;
  "sublocation"?: any;
  "startdate"?: any;
  "profile"?: any;
  "profilefull"?: any;
  "serviceMonth"?: any;
  "start_estimate"?: any;
  "comment"?: any;
  "id"?: any;
  "mechanicId"?: any;
  "superviserId"?: any;
  "busnissId"?: any;
  mechanic?: User;
  superviser?: User;
  event?: Events[];
  Route?: Route[];
  type?: Type;
  liftType?: LiftType;
  contract?: Contract;
  env?: Environment;
  business?: Business;
  servicetype?: ServiceType;
}

export class Lift implements LiftInterface {
  "elevatorn": any;
  "localn": any;
  "typeid": any;
  "lifttypeid": any;
  "address": any;
  "contractid": any;
  "servicetypeid": any;
  "busnissid": any;
  "serviceid": any;
  "mechanicid": any;
  "superviserid": any;
  "floors": any;
  "doors": any;
  "doorop": any;
  "envid": any;
  "sublocation": any;
  "startdate": any;
  "profile": any;
  "profilefull": any;
  "serviceMonth": any;
  "start_estimate": any;
  "comment": any;
  "id": any;
  "mechanicId": any;
  "superviserId": any;
  "busnissId": any;
  mechanic: User;
  superviser: User;
  event: Events[];
  Route: Route[];
  type: Type;
  liftType: LiftType;
  contract: Contract;
  env: Environment;
  business: Business;
  servicetype: ServiceType;
  constructor(data?: LiftInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Lift`.
   */
  public static getModelName() {
    return "Lift";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Lift for dynamic purposes.
  **/
  public static factory(data: LiftInterface): Lift{
    return new Lift(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Lift',
      plural: 'Lifts',
      properties: {
        "elevatorn": {
          name: 'elevatorn',
          type: 'any'
        },
        "localn": {
          name: 'localn',
          type: 'any'
        },
        "typeid": {
          name: 'typeid',
          type: 'any'
        },
        "lifttypeid": {
          name: 'lifttypeid',
          type: 'any'
        },
        "address": {
          name: 'address',
          type: 'any'
        },
        "contractid": {
          name: 'contractid',
          type: 'any'
        },
        "servicetypeid": {
          name: 'servicetypeid',
          type: 'any'
        },
        "busnissid": {
          name: 'busnissid',
          type: 'any'
        },
        "serviceid": {
          name: 'serviceid',
          type: 'any'
        },
        "mechanicid": {
          name: 'mechanicid',
          type: 'any'
        },
        "superviserid": {
          name: 'superviserid',
          type: 'any'
        },
        "floors": {
          name: 'floors',
          type: 'any'
        },
        "doors": {
          name: 'doors',
          type: 'any'
        },
        "doorop": {
          name: 'doorop',
          type: 'any'
        },
        "envid": {
          name: 'envid',
          type: 'any'
        },
        "sublocation": {
          name: 'sublocation',
          type: 'any'
        },
        "startdate": {
          name: 'startdate',
          type: 'any'
        },
        "profile": {
          name: 'profile',
          type: 'any'
        },
        "profilefull": {
          name: 'profilefull',
          type: 'any'
        },
        "serviceMonth": {
          name: 'serviceMonth',
          type: 'any'
        },
        "start_estimate": {
          name: 'start_estimate',
          type: 'any'
        },
        "comment": {
          name: 'comment',
          type: 'any'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "mechanicId": {
          name: 'mechanicId',
          type: 'any'
        },
        "superviserId": {
          name: 'superviserId',
          type: 'any'
        },
        "busnissId": {
          name: 'busnissId',
          type: 'any'
        },
      },
      relations: {
        mechanic: {
          name: 'mechanic',
          type: 'User',
          model: 'User'
        },
        superviser: {
          name: 'superviser',
          type: 'User',
          model: 'User'
        },
        event: {
          name: 'event',
          type: 'Events[]',
          model: 'Events'
        },
        Route: {
          name: 'Route',
          type: 'Route[]',
          model: 'Route'
        },
        type: {
          name: 'type',
          type: 'Type',
          model: 'Type'
        },
        liftType: {
          name: 'liftType',
          type: 'LiftType',
          model: 'LiftType'
        },
        contract: {
          name: 'contract',
          type: 'Contract',
          model: 'Contract'
        },
        env: {
          name: 'env',
          type: 'Environment',
          model: 'Environment'
        },
        business: {
          name: 'business',
          type: 'Business',
          model: 'Business'
        },
        servicetype: {
          name: 'servicetype',
          type: 'ServiceType',
          model: 'ServiceType'
        },
      }
    }
  }
}
