/* tslint:disable */
import {
  Lift,
  Events,
  Customer
} from '../index';

declare var Object: any;
export interface ContractInterface {
  "id": any;
  "number": any;
  "customerid": any;
  "building"?: any;
  "city"?: any;
  "district"?: any;
  "address"?: any;
  lift?: Lift[];
  event?: Events[];
  Customer?: Customer;
}

export class Contract implements ContractInterface {
  "id": any;
  "number": any;
  "customerid": any;
  "building": any;
  "city": any;
  "district": any;
  "address": any;
  lift: Lift[];
  event: Events[];
  Customer: Customer;
  constructor(data?: ContractInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Contract`.
   */
  public static getModelName() {
    return "Contract";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Contract for dynamic purposes.
  **/
  public static factory(data: ContractInterface): Contract{
    return new Contract(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Contract',
      plural: 'Contracts',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "number": {
          name: 'number',
          type: 'any'
        },
        "customerid": {
          name: 'customerid',
          type: 'any'
        },
        "building": {
          name: 'building',
          type: 'any'
        },
        "city": {
          name: 'city',
          type: 'any'
        },
        "district": {
          name: 'district',
          type: 'any'
        },
        "address": {
          name: 'address',
          type: 'any'
        },
      },
      relations: {
        lift: {
          name: 'lift',
          type: 'Lift[]',
          model: 'Lift'
        },
        event: {
          name: 'event',
          type: 'Events[]',
          model: 'Events'
        },
        Customer: {
          name: 'Customer',
          type: 'Customer',
          model: 'Customer'
        },
      }
    }
  }
}
