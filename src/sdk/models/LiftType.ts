/* tslint:disable */
import {
  Lift
} from '../index';

declare var Object: any;
export interface LiftTypeInterface {
  "id": any;
  "type"?: any;
  Lift?: Lift[];
}

export class LiftType implements LiftTypeInterface {
  "id": any;
  "type": any;
  Lift: Lift[];
  constructor(data?: LiftTypeInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `LiftType`.
   */
  public static getModelName() {
    return "LiftType";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of LiftType for dynamic purposes.
  **/
  public static factory(data: LiftTypeInterface): LiftType{
    return new LiftType(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'LiftType',
      plural: 'LiftTypes',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "type": {
          name: 'type',
          type: 'any'
        },
      },
      relations: {
        Lift: {
          name: 'Lift',
          type: 'Lift[]',
          model: 'Lift'
        },
      }
    }
  }
}
