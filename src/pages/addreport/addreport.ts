import { Component,ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FormControl,FormGroup,Validators} from '@angular/forms';
import { ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Transfer,TransferObject } from '@ionic-native/transfer';
import { LoopBackConfig } from '../../sdk/lb.config';
import { Slides} from 'ionic-angular';
import { ContainerApi } from '../../sdk/services/index';
import {  AddReportService  } from '../../providers/addreport-service';
import { FilePath } from '@ionic-native/file-path';
import { LocationTracker } from '../../providers/location-tracker';
declare let cordova: any;


@Component({
  selector: 'page-add-report',
  templateUrl: 'addreport.html'
})
export class AddReportPage {
  public form : any;
  public item: string;
  public Containers = [];
  public IfArrival =  [ ];
  public Causes =  [ ];
  public Resolves =  [ ];
  public IfFinish =  [ ];
  public Report=[];
  public url = LoopBackConfig.getPath()  + "/" + LoopBackConfig.getApiVersion()+"/containers/";
  public myForma2: FormGroup;
  public realIndex:number;
  public ids:number = 0;
  public flag:boolean = false;
  public datetimepicker_rep:string;
  public datetimepicker_rep1:string;
  public if_arrival:string;
  public causess:string;
  public resolves:string;
  public iffinish:string;
  public comment:string;
  public id_when:number = 4;

  city: string;
  lastImage: string = null;
  loading: Loading;
  country:string;
  public time_start_rep2:string;
  public time_end_rep2:string;
 
  //----------------------------------------------------------------------
  @ViewChild('mySlider') slider: Slides;

  constructor(public viewCtrl: ViewController,
              public navCtrl: NavController,
              public navParams: NavParams,
              public actionSheetCtrl: ActionSheetController,
              public toastCtrl: ToastController,
              public platform: Platform,
              public loadingCtrl: LoadingController,
              public camera: Camera,
              public file: File,
              public transfer: Transfer,
              public containerapi: ContainerApi,
              public addReportService:AddReportService,
              public filePath:FilePath,
              public locationTracker:LocationTracker) {
    let self = this;
    this.item = navParams.get('id');
    this.myForma2 = new FormGroup({
                datetimepicker_rep: new FormControl('',[Validators.required]),
                datetimepicker_rep1: new FormControl(Date(),[Validators.required]),
                if_arrival: new FormControl('', [Validators.required]),
                causess: new FormControl('', [Validators.required]),
                resolves: new FormControl('', [Validators.required]),               
                iffinish: new FormControl('', [Validators.required]),
                comment: new FormControl('', [Validators.required])            
           });   
   
    self.addReportService.getIfArrival(function(xxxxx: any[]){
                  self.IfArrival = xxxxx;
                               });
    self.addReportService.getIfFinish(function(xxxxx: any[]){
                  self.IfFinish = xxxxx;
                               });
    self.addReportService.getCauses(function(xxxxx: any[]){
                  self.Causes = xxxxx;
                               });
    self.addReportService.getResolves(function(xxxxx: any[]){
                  self.Resolves = xxxxx;
                               });
    self.addReportService.getReport(this.item,function(xxxxx: any){
                  self.Report = xxxxx;
                   console.log("Report =>", self.Report);
                   if (self.Report.length > 0) {
                             self.flag=true;
                             self.id_when=5;
                             self.ids = self.Report[0].id;
                             self.datetimepicker_rep =self.Report[0].data;
                             self.datetimepicker_rep1 =self.Report[0].data_end;
                             self.if_arrival =self.Report[0].start_conditions;
                             self.causess =self.Report[0].reason_event;
                             self.resolves =self.Report[0].taken_action;
                             self.iffinish =self.Report[0].end_conditions;
                                                  }

                                                  });                                                                            
    self.containerapi.getFiles(self.item)
                                    .subscribe((files) => {
                                               self.Containers = files;
                                                        });
                                         
     
     this.url=this.url+this.item;
      }

saveReport() {
     let self=this;
         let model = {id:this.ids,
                  eventid:this.item,
                  data:this.myForma2.controls['datetimepicker_rep'].value,
                  data_end:this.myForma2.controls['datetimepicker_rep1'].value,
                  start_conditions:this.myForma2.controls['if_arrival'].value,
                  end_conditions:this.myForma2.controls['iffinish'].value,
                  reason_event:this.myForma2.controls['causess'].value,
                  taken_action:this.myForma2.controls['resolves'].value,
                  comment:'ничего нет'
                                                   }; 
       self.locationTracker.putMyGeolocation(parseInt(self.item),self.id_when);
    
       if  (!this.flag) {
              this.addReportService.addReport(model,function(xxxxx: any){
                  self.Report = xxxxx;
                   console.log("Report =>", self.Report);
                   self.viewCtrl.dismiss(self.myForma2);
                               });
                 } else { 
                   
           this.addReportService.updateReport(this.ids,model,function(xxxxx: any){
                  self.Report = xxxxx;
                  console.log("Update Report =>", self.Report);
                  self.viewCtrl.dismiss(self.myForma2);
                               });
                 }                                          
  
 }
 public takePicture(sourceType) {
  // Create options for the Camera Dialog
  var options = {
    quality: 100,
    sourceType: sourceType,
    saveToPhotoAlbum: false,
    correctOrientation: true
  };
 
  // Get the data of an image
  this.camera.getPicture(options).then((imagePath) => {
    // Special handling for Android library
    if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
      this.filePath.resolveNativePath(imagePath)
        .then(filePath => {
          let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
          let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        });
    } else {
      var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }
  }, (err) => {
    this.presentToast('Error while selecting image.');
  });
}
 
// photo part
 public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Откуда фото?',
      buttons: [
        {
          text: 'Из галереи',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Взять с камеры',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Отмена',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

 // Create a new name for the image
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}
 
// Copy the image to a local folder
private copyFileToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.lastImage = newFileName;
  }, error => {
    this.presentToast('Ошибка сохранения файла.');
  });
}
 
private presentToast(text) {
  let toast = this.toastCtrl.create({
    message: text,
    duration: 3000,
    position: 'top'
  });
  toast.present();
}
 
// Always get the accurate path to your apps folder
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + img;
  }
}

public uploadImage() {
   let self = this;
  // Destination URL
  let  urlup = this.url+"/upload";
//  let  urlup1 = "https://api.lift.kz:443/api/containers/49/upload" 

  // File for Upload
  let targetPath = this.pathForImage(this.lastImage);
 
  // File name only
  let filename = this.lastImage;
  console.log('Вычисляем url ' + urlup);
  console.log('targetPath ' + targetPath);
  console.log('filename ' + filename);
  let options = {
    fileKey: "file",
    fileName: filename,
    trustEveryone: true,
    chunkedMode: true,
    mimeType: "multipart/form-data",
    params : {'fileName': filename}
  };
 
  const fileTransfer: TransferObject = this.transfer.create();
 
  this.loading = this.loadingCtrl.create({
    content: 'Загрузка фото на сервер...',
  });
  this.loading.present();
 

  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath, urlup, options,true).then(data => {
    this.loading.dismissAll()
    this.presentToast('Фото успешно  загружено.');
        self.containerapi.getFiles(self.item)
                                            .subscribe((files) => {
                                                                   self.Containers = files;
                                                              
                                                                  });
  }, err => {
    self.loading.dismissAll()
    self.presentToast('При загрузке возникла ошибка, повторите');
  }); 
}


removePhoto() {
  let self = this;
  self.realIndex = this.slider.getActiveIndex();
  self.containerapi.removeFile(self.item,self.Containers[self.realIndex-1].name)
                                    .subscribe((files) => {
                                              self.containerapi.getFiles(self.item)
                                                          .subscribe((files) => {
                                                                   self.Containers = files;
                                                                 
                                                                  });
                                                             });

  console.log("realIndex =>",this.realIndex);
  }

}
