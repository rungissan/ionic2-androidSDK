/* tslint:disable */
import {
  Lift
} from '../index';

declare var Object: any;
export interface ServiceTypeInterface {
  "id": any;
  "type": any;
  Lift?: Lift[];
}

export class ServiceType implements ServiceTypeInterface {
  "id": any;
  "type": any;
  Lift: Lift[];
  constructor(data?: ServiceTypeInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `ServiceType`.
   */
  public static getModelName() {
    return "ServiceType";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of ServiceType for dynamic purposes.
  **/
  public static factory(data: ServiceTypeInterface): ServiceType{
    return new ServiceType(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'ServiceType',
      plural: 'ServiceTypes',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "type": {
          name: 'type',
          type: 'any'
        },
      },
      relations: {
        Lift: {
          name: 'Lift',
          type: 'Lift[]',
          model: 'Lift'
        },
      }
    }
  }
}
