import {Component,ViewChild} from '@angular/core';
import { Platform,MenuController, Nav} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { UserApi }    from '../../sdk/services/index';
import { User}  from '../../sdk/models/User';
import { AlarmsPage } from '../alarms/alarms';
import { ReposPage } from '../repos/repos';
import { SettingsPage } from '../settings/settings';
import { OrganisationsPage } from '../organisations/organisations';
import { MapsPage } from '../maps/maps';
declare let FCMPlugin: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
 username:string ='';
 email:string ='';
 user:User;
 public showfooter=true;
 
 // make AlarmsPage the root (or first) page
@ViewChild(Nav) nav: Nav;
 rootPage: any =AlarmsPage ;
  pages: Array<{title: string, component: any}>;
 constructor(private userApi: UserApi,
             private platform: Platform,
             public menu: MenuController) {
               // set our app's pages
               this.pages = [
                 { title: 'Все аварии', component: AlarmsPage },
                 { title: 'На карте', component: MapsPage },
                 { title: 'Плановые работы', component: ReposPage },
                 { title: 'Справочники', component: OrganisationsPage },
                 { title: 'Настройки', component: SettingsPage }
                                  ];
             
             this.user=<User>this.userApi.getCachedCurrent();
          //   console.log('this.user',this.user);
             if (this.user===undefined) {
                                   this.user.username=null;
                                   this.showfooter=false;
                                   this.nav.setRoot(LoginPage);
                                 }
           
             this.username = this.user.username;
             this.email = this.user.email; 
                this.platform.ready().then(() => {
                      if(typeof(FCMPlugin) !== "undefined"){
                      FCMPlugin.subscribeToTopic(this.user.id);
                                     }
                                  });
                                 };
   
  public logout() {
    this.userApi.logout().subscribe(succ => {
        this.user.username=null;
        this.showfooter=false;
        this.nav.setRoot(LoginPage);
    });
  }
  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
  
}
