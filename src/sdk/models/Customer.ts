/* tslint:disable */
import {
  User,
  Contract
} from '../index';

declare var Object: any;
export interface CustomerInterface {
  "id": any;
  "name": any;
  "userid": any;
  "address"?: any;
  "rc"?: any;
  "kbe"?: any;
  "rnn"?: any;
  "bin"?: any;
  "bank"?: any;
  "bik"?: any;
  "userId"?: any;
  user?: User;
  Contract?: Contract[];
}

export class Customer implements CustomerInterface {
  "id": any;
  "name": any;
  "userid": any;
  "address": any;
  "rc": any;
  "kbe": any;
  "rnn": any;
  "bin": any;
  "bank": any;
  "bik": any;
  "userId": any;
  user: User;
  Contract: Contract[];
  constructor(data?: CustomerInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Customer`.
   */
  public static getModelName() {
    return "Customer";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Customer for dynamic purposes.
  **/
  public static factory(data: CustomerInterface): Customer{
    return new Customer(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Customer',
      plural: 'Customers',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "name": {
          name: 'name',
          type: 'any'
        },
        "userid": {
          name: 'userid',
          type: 'any'
        },
        "address": {
          name: 'address',
          type: 'any'
        },
        "rc": {
          name: 'rc',
          type: 'any'
        },
        "kbe": {
          name: 'kbe',
          type: 'any'
        },
        "rnn": {
          name: 'rnn',
          type: 'any'
        },
        "bin": {
          name: 'bin',
          type: 'any'
        },
        "bank": {
          name: 'bank',
          type: 'any'
        },
        "bik": {
          name: 'bik',
          type: 'any'
        },
        "userId": {
          name: 'userId',
          type: 'any'
        },
      },
      relations: {
        user: {
          name: 'user',
          type: 'User',
          model: 'User'
        },
        Contract: {
          name: 'Contract',
          type: 'Contract[]',
          model: 'Contract'
        },
      }
    }
  }
}
