/* tslint:disable */

declare var Object: any;
export interface NotificationInterface {
  "alert"?: any;
  "badge"?: any;
  "category"?: any;
  "collapseKey"?: any;
  "contentAvailable"?: any;
  "created"?: any;
  "delayWhileIdle"?: any;
  "deviceToken": any;
  "deviceType": any;
  "expirationInterval"?: any;
  "expirationTime"?: any;
  "modified"?: any;
  "scheduledTime"?: any;
  "sound"?: any;
  "status"?: any;
  "urlArgs"?: any;
  "id"?: any;
}

export class Notification implements NotificationInterface {
  "alert": any;
  "badge": any;
  "category": any;
  "collapseKey": any;
  "contentAvailable": any;
  "created": any;
  "delayWhileIdle": any;
  "deviceToken": any;
  "deviceType": any;
  "expirationInterval": any;
  "expirationTime": any;
  "modified": any;
  "scheduledTime": any;
  "sound": any;
  "status": any;
  "urlArgs": any;
  "id": any;
  constructor(data?: NotificationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Notification`.
   */
  public static getModelName() {
    return "Notification";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Notification for dynamic purposes.
  **/
  public static factory(data: NotificationInterface): Notification{
    return new Notification(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Notification',
      plural: 'Notifications',
      properties: {
        "alert": {
          name: 'alert',
          type: 'any'
        },
        "badge": {
          name: 'badge',
          type: 'any'
        },
        "category": {
          name: 'category',
          type: 'any'
        },
        "collapseKey": {
          name: 'collapseKey',
          type: 'any'
        },
        "contentAvailable": {
          name: 'contentAvailable',
          type: 'any'
        },
        "created": {
          name: 'created',
          type: 'any'
        },
        "delayWhileIdle": {
          name: 'delayWhileIdle',
          type: 'any'
        },
        "deviceToken": {
          name: 'deviceToken',
          type: 'any'
        },
        "deviceType": {
          name: 'deviceType',
          type: 'any'
        },
        "expirationInterval": {
          name: 'expirationInterval',
          type: 'any'
        },
        "expirationTime": {
          name: 'expirationTime',
          type: 'any'
        },
        "modified": {
          name: 'modified',
          type: 'any'
        },
        "scheduledTime": {
          name: 'scheduledTime',
          type: 'any'
        },
        "sound": {
          name: 'sound',
          type: 'any'
        },
        "status": {
          name: 'status',
          type: 'any'
        },
        "urlArgs": {
          name: 'urlArgs',
          type: 'any'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
