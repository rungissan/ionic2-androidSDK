import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LocationTracker } from '../../providers/location-tracker';
import { Geolocation } from '@ionic-native/geolocation';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Http } from '@angular/http';
import { GeoApi }  from '../../sdk/services/index';
import 'rxjs/add/operator/map';


declare var google;
@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html'
})
export class MapsPage {
 @ViewChild('map') mapElement: ElementRef;
  map: any;
  public time:number;
 
  constructor(public navCtrl: NavController,
              public locationTracker: LocationTracker,
              public http: Http,
              public geolocation:Geolocation,
              public geoApi:GeoApi,
              public backgroundGeolocation: BackgroundGeolocation,
              public zone:NgZone) {
      this.locationTracker.lat= 51.165977;      
      this.locationTracker.lng=71.427608;
  }
 
  ionViewDidLoad(){
    this.loadMap();
    this.getMarkers();
  }

 
  loadMap(){
     let latLng = new google.maps.LatLng(this.locationTracker.lat, this.locationTracker.lng);
 
    let mapOptions = {
      center: latLng,
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
 
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
  
  /*
  let self=this;
     // Background Tracking
  

  let config = {
    desiredAccuracy: 0,
    stationaryRadius: 20,
    distanceFilter: 10, 
    debug: true,
    interval: 2000 
  };
 
  this.backgroundGeolocation.configure(config).subscribe((location) => {
 
    console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);
 
    // Run update inside of Angular's zone
    self.zone.run(() => {
     
      console.log('координаты бэкграунд'+location.latitude+' '+location.longitude+'  '+location.time );
     
     let latLng = new google.maps.LatLng(location.latitude, location.longitude);

     let marker = new google.maps.Marker({
      map: this.map,
      icon: new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
      new google.maps.Size(22, 22),
      new google.maps.Point(0, 18),
      new google.maps.Point(11, 11)),
    position: latLng
     });

     let content = "<h4>You are here</h4>";
     self.addInfoWindow(marker, content);

 
  })
 
 });
 self.backgroundGeolocation.start();
*/
}
 
 addMarker(){
 
  let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: this.map.getCenter()
  });
 
  let content = "<h4>Лифт № 7 по улице Чайковкого!</h4>";          
 
  this.addInfoWindow(marker, content);
 
}
//******************************************************
addInfoWindow(marker, content){
 
  let infoWindow = new google.maps.InfoWindow({
    content: content
  });
 
  google.maps.event.addListener(marker, 'click', () => {
    infoWindow.open(this.map, marker);
  });
 }
 getMarkers() {
    this.http.get('assets/data/markers.json')
    .map((res) => res.json())
    .subscribe(data => {
      this.addMarkersToMap(data);
    });
  }

  addMarkersToMap(markers) {
    for(let marker of markers) {
      var position = new google.maps.LatLng(marker.latitude, marker.longitude);
      var dogwalkMarker = new google.maps.Marker({position: position, title: marker.name});
      this.addInfoWindow(dogwalkMarker, marker.name);
      dogwalkMarker.setMap(this.map);
    }
  }

//******************************************************

}


