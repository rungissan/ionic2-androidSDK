import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { AccessToken }  from '../../sdk/models/index';
import { UserApi,LoopBackAuth} from '../../sdk/services/index';

 
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loading: Loading;
  accessTokenId: string;
  footerhide=true;
  registerCredentials = {email: '', password:''};
  constructor(private nav: NavController,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              protected auth: LoopBackAuth,
              private userApi: UserApi) {
    this.accessTokenId=this.auth.getAccessTokenId();
   if (this.accessTokenId) {
        console.log('this.accessTokenId',this.accessTokenId);
        this.nav.setRoot(HomePage);
      }
}
 
  public createAccount() {
    this.nav.push(RegisterPage);
  }
  public login() {
    this.showLoading();
    let self=this;
    console.log(self.registerCredentials.email+' +++  '+self.registerCredentials.password);
    this.userApi.login(self.registerCredentials).subscribe((token: AccessToken) => {
                                          console.log("Токен есть :"+token)
                                          self.loading.dismiss();
                                          this.nav.setRoot(HomePage);
                                          },
                                          (error) => {
                                                      self.loading.dismiss();
                                                      this.showError("Не возможно подключиться");
                                                     });
             
  }
 
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Идет проверка личности..'
    });
    this.loading.present();
  }
 
  showError(text) {
    setTimeout(() => {
      this.loading.dismiss();
    },1000);
 
    let alert = this.alertCtrl.create({
      title: 'Войти не удалось',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}

