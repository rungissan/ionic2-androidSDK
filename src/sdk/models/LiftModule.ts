/* tslint:disable */

declare var Object: any;
export interface LiftModuleInterface {
  "id": any;
  "module": any;
}

export class LiftModule implements LiftModuleInterface {
  "id": any;
  "module": any;
  constructor(data?: LiftModuleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `LiftModule`.
   */
  public static getModelName() {
    return "LiftModule";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of LiftModule for dynamic purposes.
  **/
  public static factory(data: LiftModuleInterface): LiftModule{
    return new LiftModule(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'LiftModule',
      plural: 'LiftModules',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "module": {
          name: 'module',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
