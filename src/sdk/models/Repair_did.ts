/* tslint:disable */
import {
  Reports
} from '../index';

declare var Object: any;
export interface Repair_didInterface {
  "id": any;
  "usl": any;
  Report?: Reports[];
}

export class Repair_did implements Repair_didInterface {
  "id": any;
  "usl": any;
  Report: Reports[];
  constructor(data?: Repair_didInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Repair_did`.
   */
  public static getModelName() {
    return "Repair_did";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Repair_did for dynamic purposes.
  **/
  public static factory(data: Repair_didInterface): Repair_did{
    return new Repair_did(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Repair_did',
      plural: 'Repair_dids',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "usl": {
          name: 'usl',
          type: 'any'
        },
      },
      relations: {
        Report: {
          name: 'Report',
          type: 'Reports[]',
          model: 'Reports'
        },
      }
    }
  }
}
