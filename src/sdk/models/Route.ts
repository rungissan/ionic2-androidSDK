/* tslint:disable */
import {
  User
} from '../index';

declare var Object: any;
export interface RouteInterface {
  "id": any;
  "liftid": any;
  "userid": any;
  "realTime": any;
  "realDate": any;
  "userId"?: any;
  mechanic?: User;
}

export class Route implements RouteInterface {
  "id": any;
  "liftid": any;
  "userid": any;
  "realTime": any;
  "realDate": any;
  "userId": any;
  mechanic: User;
  constructor(data?: RouteInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Route`.
   */
  public static getModelName() {
    return "Route";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Route for dynamic purposes.
  **/
  public static factory(data: RouteInterface): Route{
    return new Route(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Route',
      plural: 'Routes',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "liftid": {
          name: 'liftid',
          type: 'any'
        },
        "userid": {
          name: 'userid',
          type: 'any'
        },
        "realTime": {
          name: 'realTime',
          type: 'any'
        },
        "realDate": {
          name: 'realDate',
          type: 'any'
        },
        "userId": {
          name: 'userId',
          type: 'any'
        },
      },
      relations: {
        mechanic: {
          name: 'mechanic',
          type: 'User',
          model: 'User'
        },
      }
    }
  }
}
