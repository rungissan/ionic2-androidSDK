import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-details',
  templateUrl: 'details.html'
})
export class DetailsPage {
  pushMessage: string = "push message will be displayed here";
  constructor(public navCtrl: NavController, public navParams: NavParams) {
      if (navParams.data.message) {
      this.pushMessage = navParams.data.message;
    }
}

}
