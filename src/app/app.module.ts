import {NgModule,ErrorHandler } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
import {IonicApp, IonicModule,IonicErrorHandler} from "ionic-angular";
import {ConnectKZ} from "./app";

//pages
import {DetailsPage} from "../pages/details/details";
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';
import { AlarmsPage } from '../pages/alarms/alarms';
import { AddReportPage } from '../pages/addreport/addreport';
import { ReposPage } from '../pages/repos/repos';
import { OrganisationsPage } from '../pages/organisations/organisations';
import { AlarmDetailsPage } from '../pages/alarm-details/alarm-details';
import { SettingsPage } from '../pages/settings/settings';
import { MapsPage } from '../pages/maps/maps';

//services
import { AlarmsService } from '../providers/alarm-services';
import { SDKBrowserModule as SDKModule } from '../sdk/index';
import { LocationTracker } from '../providers/location-tracker';
import { DopService } from '../providers/dop-service'; 
import { BackgroundMode } from '@ionic-native/background-mode';
import { MediaPlugin } from '@ionic-native/media';
import { File } from '@ionic-native/file';
import { LocalNotifications } from '@ionic-native/local-notifications';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Deeplinks} from '@ionic-native/deeplinks';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { Transfer } from '@ionic-native/transfer';
import {  AddReportService  } from '../providers/addreport-service';
import { FilePath } from '@ionic-native/file-path';

@NgModule({
  declarations: [
    ConnectKZ,
    HomePage,
    DetailsPage,
    LoginPage,
    AlarmsPage,
    AlarmDetailsPage,
    ReposPage,
    SettingsPage,
    OrganisationsPage,
    RegisterPage,
    MapsPage,
    AddReportPage
  ],
  imports: [
    IonicModule.forRoot(ConnectKZ),
    SDKModule.forRoot(),
    BrowserModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConnectKZ,
    HomePage,
    DetailsPage,
    LoginPage,
    AlarmsPage,
    AlarmDetailsPage,
    ReposPage,
    OrganisationsPage,
    MapsPage,
    SettingsPage,
    RegisterPage,
    AddReportPage
  ],
  providers: [LocationTracker,
              AlarmsService,
              AddReportService,
              BackgroundMode,
              File,
              LocalNotifications,
              MediaPlugin,
              StatusBar,
              SplashScreen,
              Deeplinks,
              BackgroundGeolocation,
              Geolocation,
              FilePath,
              Camera,
              Transfer,
              DopService,
              {provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {
}
