/* tslint:disable */
import { Injectable } from '@angular/core';
import { User } from '../../models/User';
import { Lift } from '../../models/Lift';
import { Events } from '../../models/Events';
import { Recoverylist } from '../../models/Recoverylist';
import { Troubles } from '../../models/Troubles';
import { Route } from '../../models/Route';
import { Type } from '../../models/Type';
import { LiftType } from '../../models/LiftType';
import { LiftModule } from '../../models/LiftModule';
import { Contract } from '../../models/Contract';
import { Customer } from '../../models/Customer';
import { Environment } from '../../models/Environment';
import { Business } from '../../models/Business';
import { ServiceType } from '../../models/ServiceType';
import { Liftelements } from '../../models/Liftelements';
import { Cause_alarm } from '../../models/Cause_alarm';
import { Repair_did } from '../../models/Repair_did';
import { Reports } from '../../models/Reports';
import { Usl_arrival } from '../../models/Usl_arrival';
import { Usl_finish } from '../../models/Usl_finish';
import { Geo } from '../../models/Geo';
import { Modules } from '../../models/Modules';
import { ModuleElem } from '../../models/ModuleElem';
import { Container } from '../../models/Container';
import { Push } from '../../models/Push';
import { Notification } from '../../models/Notification';
import { Application } from '../../models/Application';
import { Installation } from '../../models/Installation';
import { Email } from '../../models/Email';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    User: User,
    Lift: Lift,
    Events: Events,
    Recoverylist: Recoverylist,
    Troubles: Troubles,
    Route: Route,
    Type: Type,
    LiftType: LiftType,
    LiftModule: LiftModule,
    Contract: Contract,
    Customer: Customer,
    Environment: Environment,
    Business: Business,
    ServiceType: ServiceType,
    Liftelements: Liftelements,
    Cause_alarm: Cause_alarm,
    Repair_did: Repair_did,
    Reports: Reports,
    Usl_arrival: Usl_arrival,
    Usl_finish: Usl_finish,
    Geo: Geo,
    Modules: Modules,
    ModuleElem: ModuleElem,
    Container: Container,
    Push: Push,
    Notification: Notification,
    Application: Application,
    Installation: Installation,
    Email: Email,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
