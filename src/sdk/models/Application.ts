/* tslint:disable */

declare var Object: any;
export interface ApplicationInterface {
  "id"?: any;
  "realm"?: any;
  "name": any;
  "description"?: any;
  "icon"?: any;
  "owner"?: any;
  "collaborators"?: any;
  "email"?: any;
  "emailVerified"?: any;
  "url"?: any;
  "callbackUrls"?: any;
  "permissions"?: any;
  "clientKey"?: any;
  "javaScriptKey"?: any;
  "restApiKey"?: any;
  "windowsKey"?: any;
  "masterKey"?: any;
  "pushSettings"?: any;
  "authenticationEnabled"?: any;
  "anonymousAllowed"?: any;
  "authenticationSchemes"?: any;
  "status"?: any;
  "created"?: any;
  "modified"?: any;
}

export class Application implements ApplicationInterface {
  "id": any;
  "realm": any;
  "name": any;
  "description": any;
  "icon": any;
  "owner": any;
  "collaborators": any;
  "email": any;
  "emailVerified": any;
  "url": any;
  "callbackUrls": any;
  "permissions": any;
  "clientKey": any;
  "javaScriptKey": any;
  "restApiKey": any;
  "windowsKey": any;
  "masterKey": any;
  "pushSettings": any;
  "authenticationEnabled": any;
  "anonymousAllowed": any;
  "authenticationSchemes": any;
  "status": any;
  "created": any;
  "modified": any;
  constructor(data?: ApplicationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Application`.
   */
  public static getModelName() {
    return "Application";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Application for dynamic purposes.
  **/
  public static factory(data: ApplicationInterface): Application{
    return new Application(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Application',
      plural: 'Applications',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "realm": {
          name: 'realm',
          type: 'any'
        },
        "name": {
          name: 'name',
          type: 'any'
        },
        "description": {
          name: 'description',
          type: 'any'
        },
        "icon": {
          name: 'icon',
          type: 'any'
        },
        "owner": {
          name: 'owner',
          type: 'any'
        },
        "collaborators": {
          name: 'collaborators',
          type: 'any'
        },
        "email": {
          name: 'email',
          type: 'any'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'any'
        },
        "url": {
          name: 'url',
          type: 'any'
        },
        "callbackUrls": {
          name: 'callbackUrls',
          type: 'any'
        },
        "permissions": {
          name: 'permissions',
          type: 'any'
        },
        "clientKey": {
          name: 'clientKey',
          type: 'any'
        },
        "javaScriptKey": {
          name: 'javaScriptKey',
          type: 'any'
        },
        "restApiKey": {
          name: 'restApiKey',
          type: 'any'
        },
        "windowsKey": {
          name: 'windowsKey',
          type: 'any'
        },
        "masterKey": {
          name: 'masterKey',
          type: 'any'
        },
        "pushSettings": {
          name: 'pushSettings',
          type: 'any'
        },
        "authenticationEnabled": {
          name: 'authenticationEnabled',
          type: 'any',
          default: true
        },
        "anonymousAllowed": {
          name: 'anonymousAllowed',
          type: 'any',
          default: true
        },
        "authenticationSchemes": {
          name: 'authenticationSchemes',
          type: 'any'
        },
        "status": {
          name: 'status',
          type: 'any',
          default: 'sandbox'
        },
        "created": {
          name: 'created',
          type: 'any'
        },
        "modified": {
          name: 'modified',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
