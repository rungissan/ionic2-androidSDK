import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BackgroundMode } from '@ionic-native/background-mode';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  public isBackGrounded: boolean;
  public isBackGroundold: boolean;

  constructor(public navCtrl: NavController,
              private backgroundMode: BackgroundMode ) {
     this.isBackGrounded = this.backgroundMode.isEnabled();
     this.isBackGroundold = this.isBackGrounded;
  }
  public notify() {
     console.log('start notify start notify start notify start notify ');
     if (this.isBackGrounded!=this.isBackGroundold) {
                                            console.log('start1 notify1 start1 notify1 start1 notify1 start1 notify1 ');
                                            if (this.isBackGrounded) {
                                                              console.log('enable enable enable ');
                                                              this.backgroundMode.enable();
                                                              this.isBackGrounded=true;
                                                              this.isBackGroundold =true;
                                                                  } else {
                                                                 console.log('disable disable disable :' + this.isBackGrounded );
                                                                 this.backgroundMode.disable();
                                                                 this.isBackGrounded=false;
                                                                 this.isBackGroundold =false;
                                                                 }
                                              }
     
   }
  ionViewDidLoad() {
    console.log('Hello SettingsPage Page');
  }
}
