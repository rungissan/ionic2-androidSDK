/* tslint:disable */
import {
  Lift,
  Events,
  Route,
  Customer
} from '../index';

declare var Object: any;
export interface UserInterface {
  "id": any;
  "realm"?: any;
  "username"?: any;
  "password"?: any;
  "email"?: any;
  "emailVerified"?: any;
  "verificaTiontoken"?: any;
  "status"?: any;
  "created"?: any;
  "modified"?: any;
  "avatarurl"?: any;
  "tel"?: any;
  "roleId"?: any;
  "verificationToken"?: any;
  accessTokens?: any[];
  Role?: any[];
  Mechanic?: Lift[];
  Superviser?: Lift[];
  Event?: Events[];
  Route?: Route[];
  Customer?: Customer[];
}

export class User implements UserInterface {
  "id": any;
  "realm": any;
  "username": any;
  "password": any;
  "email": any;
  "emailVerified": any;
  "verificaTiontoken": any;
  "status": any;
  "created": any;
  "modified": any;
  "avatarurl": any;
  "tel": any;
  "roleId": any;
  "verificationToken": any;
  accessTokens: any[];
  Role: any[];
  Mechanic: Lift[];
  Superviser: Lift[];
  Event: Events[];
  Route: Route[];
  Customer: Customer[];
  constructor(data?: UserInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `User`.
   */
  public static getModelName() {
    return "User";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of User for dynamic purposes.
  **/
  public static factory(data: UserInterface): User{
    return new User(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'User',
      plural: 'Users',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "realm": {
          name: 'realm',
          type: 'any'
        },
        "username": {
          name: 'username',
          type: 'any'
        },
        "password": {
          name: 'password',
          type: 'any'
        },
        "email": {
          name: 'email',
          type: 'any'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'any'
        },
        "verificaTiontoken": {
          name: 'verificaTiontoken',
          type: 'any'
        },
        "status": {
          name: 'status',
          type: 'any',
          default: 'active'
        },
        "created": {
          name: 'created',
          type: 'any'
        },
        "modified": {
          name: 'modified',
          type: 'any'
        },
        "avatarurl": {
          name: 'avatarurl',
          type: 'any'
        },
        "tel": {
          name: 'tel',
          type: 'any'
        },
        "roleId": {
          name: 'roleId',
          type: 'any',
          default: 5
        },
        "verificationToken": {
          name: 'verificationToken',
          type: 'any'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: ''
        },
        Role: {
          name: 'Role',
          type: 'any[]',
          model: ''
        },
        Mechanic: {
          name: 'Mechanic',
          type: 'Lift[]',
          model: 'Lift'
        },
        Superviser: {
          name: 'Superviser',
          type: 'Lift[]',
          model: 'Lift'
        },
        Event: {
          name: 'Event',
          type: 'Events[]',
          model: 'Events'
        },
        Route: {
          name: 'Route',
          type: 'Route[]',
          model: 'Route'
        },
        Customer: {
          name: 'Customer',
          type: 'Customer[]',
          model: 'Customer'
        },
      }
    }
  }
}
