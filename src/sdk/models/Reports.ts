/* tslint:disable */

declare var Object: any;
export interface ReportsInterface {
  "id": any;
  "eventid": any;
  "data": any;
  "data_end": any;
  "start_conditions": any;
  "end_conditions": any;
  "reason_event": any;
  "taken_action": any;
  "comment": any;
}

export class Reports implements ReportsInterface {
  "id": any;
  "eventid": any;
  "data": any;
  "data_end": any;
  "start_conditions": any;
  "end_conditions": any;
  "reason_event": any;
  "taken_action": any;
  "comment": any;
  constructor(data?: ReportsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Reports`.
   */
  public static getModelName() {
    return "Reports";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Reports for dynamic purposes.
  **/
  public static factory(data: ReportsInterface): Reports{
    return new Reports(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Reports',
      plural: 'Reports',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "eventid": {
          name: 'eventid',
          type: 'any'
        },
        "data": {
          name: 'data',
          type: 'any'
        },
        "data_end": {
          name: 'data_end',
          type: 'any'
        },
        "start_conditions": {
          name: 'start_conditions',
          type: 'any'
        },
        "end_conditions": {
          name: 'end_conditions',
          type: 'any'
        },
        "reason_event": {
          name: 'reason_event',
          type: 'any'
        },
        "taken_action": {
          name: 'taken_action',
          type: 'any'
        },
        "comment": {
          name: 'comment',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
