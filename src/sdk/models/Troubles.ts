/* tslint:disable */

declare var Object: any;
export interface TroublesInterface {
  "id": any;
  "error_kod": any;
  "description": any;
  "solution": any;
}

export class Troubles implements TroublesInterface {
  "id": any;
  "error_kod": any;
  "description": any;
  "solution": any;
  constructor(data?: TroublesInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Troubles`.
   */
  public static getModelName() {
    return "Troubles";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Troubles for dynamic purposes.
  **/
  public static factory(data: TroublesInterface): Troubles{
    return new Troubles(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Troubles',
      plural: 'Troubles',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "error_kod": {
          name: 'error_kod',
          type: 'any'
        },
        "description": {
          name: 'description',
          type: 'any'
        },
        "solution": {
          name: 'solution',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
