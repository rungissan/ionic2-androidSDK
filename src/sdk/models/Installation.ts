/* tslint:disable */

declare var Object: any;
export interface InstallationInterface {
  "appId": any;
  "appVersion"?: any;
  "badge"?: any;
  "created"?: any;
  "deviceToken": any;
  "deviceType": any;
  "modified"?: any;
  "status"?: any;
  "subscriptions"?: any;
  "timeZone"?: any;
  "userId"?: any;
  "id"?: any;
}

export class Installation implements InstallationInterface {
  "appId": any;
  "appVersion": any;
  "badge": any;
  "created": any;
  "deviceToken": any;
  "deviceType": any;
  "modified": any;
  "status": any;
  "subscriptions": any;
  "timeZone": any;
  "userId": any;
  "id": any;
  constructor(data?: InstallationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Installation`.
   */
  public static getModelName() {
    return "Installation";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Installation for dynamic purposes.
  **/
  public static factory(data: InstallationInterface): Installation{
    return new Installation(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Installation',
      plural: 'Installations',
      properties: {
        "appId": {
          name: 'appId',
          type: 'any'
        },
        "appVersion": {
          name: 'appVersion',
          type: 'any'
        },
        "badge": {
          name: 'badge',
          type: 'any'
        },
        "created": {
          name: 'created',
          type: 'any'
        },
        "deviceToken": {
          name: 'deviceToken',
          type: 'any'
        },
        "deviceType": {
          name: 'deviceType',
          type: 'any'
        },
        "modified": {
          name: 'modified',
          type: 'any'
        },
        "status": {
          name: 'status',
          type: 'any'
        },
        "subscriptions": {
          name: 'subscriptions',
          type: 'any'
        },
        "timeZone": {
          name: 'timeZone',
          type: 'any'
        },
        "userId": {
          name: 'userId',
          type: 'any'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
