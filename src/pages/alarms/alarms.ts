import { Component } from '@angular/core';
import { NavController,LoadingController, Loading} from 'ionic-angular';
import { Events } from '../../sdk/models/Events';
import { AlarmDetailsPage } from '../alarm-details/alarm-details';
import {  AlarmsService } from '../../providers/alarm-services';

@Component({
  selector: 'page-alarms',
  templateUrl: 'alarms.html'
})
export class AlarmsPage {
  alarms: Events[];
  loading: Loading;
  public alarmss:Events[];
  originalAlarms: Events[];
  constructor(public navCtrl: NavController, private alarmsService: AlarmsService,private loadingCtrl: LoadingController) {
  this.showLoading();
  let self = this;
  self.alarmsService.getEvents(function(xxxxx: any[]){
            self.alarmss = xxxxx;
            self.originalAlarms = xxxxx;
            self.loading.dismiss();
                     });
   }
  goToDetails(item: any) {
      this.navCtrl.push(AlarmDetailsPage, {item});
  }
 showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Идет загрузка..'
    });
    this.loading.present();
  }
 search(searchEvent) {
    let term = searchEvent.target.value;
    console.log('вася',term);
    // We will only perform the search if we have 3 or more characters
    if (term.trim() === '' || term.trim().length < 3) {
           this.alarms = this.originalAlarms;
    } else {
            this.alarms = this.alarms;
      };
    }
   
}


