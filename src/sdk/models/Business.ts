/* tslint:disable */
import {
  Lift
} from '../index';

declare var Object: any;
export interface BusinessInterface {
  "id": any;
  "type": any;
  Lift?: Lift[];
}

export class Business implements BusinessInterface {
  "id": any;
  "type": any;
  Lift: Lift[];
  constructor(data?: BusinessInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Business`.
   */
  public static getModelName() {
    return "Business";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Business for dynamic purposes.
  **/
  public static factory(data: BusinessInterface): Business{
    return new Business(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Business',
      plural: 'Businesses',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "type": {
          name: 'type',
          type: 'any'
        },
      },
      relations: {
        Lift: {
          name: 'Lift',
          type: 'Lift[]',
          model: 'Lift'
        },
      }
    }
  }
}
