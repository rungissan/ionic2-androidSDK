import { Injectable} from '@angular/core';
import { EventsApi }  from '../sdk/services/index';
import { Usl_arrivalApi }  from '../sdk/services/index';
import { Usl_finishApi }  from '../sdk/services/index';
import { Repair_didApi }  from '../sdk/services/index';
import { Cause_alarmApi }  from '../sdk/services/index';
import { ReportsApi }  from '../sdk/services/index';

import { ToastController } from 'ionic-angular';
//import * as moment from 'moment';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class AddReportService {
   IfFinish : any [];
   Resolves : any [];
   Causes: any [];
   IfArrival: any [ ];
   Report:any [];
  base_time = new Date('1970-01-01T00:00:00.000Z');
  constructor(private usl_finishApi: Usl_finishApi,
              private eventsApi: EventsApi,
              private repair_didApi:Repair_didApi,
              private cause_alarmApi: Cause_alarmApi,
              private usl_arrivalApi:Usl_arrivalApi,
              private toastCtrl: ToastController,
              private reportsApi:ReportsApi) {
  console.log(this.base_time);};
  reformatData = function (element:any) {
                   return element;
           };
   
    getIfArrival(cb:any):any[] {
       this.usl_arrivalApi
         .find()
         .subscribe(function(response: any) {
           this.IfArrival=response;
           cb(this.IfArrival);
         });
      return  this.IfArrival;
     }
      getIfFinish(cb:any):any[] {
       this.usl_finishApi
         .find()
         .subscribe(function(response: any) {
           this.IfFinish=response;
           cb(this.IfFinish);
         });
      return  this.IfFinish;
     }
    getResolves(cb:any):any[] {
       this.repair_didApi
         .find()
         .subscribe(function(response: any) {
           this.Resolves=response;
           cb(this.Resolves);
         });
      return  this.Resolves;
     }
      getCauses(cb:any):any[] {
       this.cause_alarmApi
         .find()
         .subscribe(function(response: any) {
           this.Causes=response;
           cb(this.Causes);
         });
      return  this.Causes;
     }
  
  getReport(item:string,cb:any):any[] {
  let self=this;
     this.reportsApi
        .find({ 
                 where: {eventid : item  }
             }
             )        
        .subscribe(function(response: any) {
          // Process response
          self.presentToast('Данные  успешненько получены!');
           let doubles=response.map(self.reformatData);
           self.Report=doubles;
           cb(response);
         },
          (error) => {self.presentToast('Не удалось получить данные!');
                      cb( self.Report ); });
       return  self.Report;
  }
  updateReport(item:number,model:any,cb:any):any[] {
      let self=this;
      this.reportsApi
        .patchAttributes(item, model)
        .subscribe(function(response: any) {
          // Process response
          self.presentToast('Данные успешно обновлены!');
          cb(self.Report);
         },
         (error) => {self.presentToast('Не обновить отчет!');});
        return  self.Report;
   }
   addReport(model: any,cb:any) {
    let self=this;
     this.reportsApi
       .create(model)
       .subscribe(function(response: any) {
       self.presentToast('отчет добавлен успешно!');
       cb(response);
    },
        (error) => {self.presentToast('Не удалось добавить отчет!');});
         };
 
   presentToast(message:string) {
   let toast =this.toastCtrl.create({
    message: message,
    duration: 3000,
    position: 'top'
  });
  toast.present();
  }
  }
