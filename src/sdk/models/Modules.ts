/* tslint:disable */
import {
  Liftelements
} from '../index';

declare var Object: any;
export interface ModulesInterface {
  "id": any;
  "name": any;
  liftelem?: Liftelements[];
}

export class Modules implements ModulesInterface {
  "id": any;
  "name": any;
  liftelem: Liftelements[];
  constructor(data?: ModulesInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Modules`.
   */
  public static getModelName() {
    return "Modules";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Modules for dynamic purposes.
  **/
  public static factory(data: ModulesInterface): Modules{
    return new Modules(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Modules',
      plural: 'Modules',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "name": {
          name: 'name',
          type: 'any'
        },
      },
      relations: {
        liftelem: {
          name: 'liftelem',
          type: 'Liftelements[]',
          model: 'Liftelements'
        },
      }
    }
  }
}
