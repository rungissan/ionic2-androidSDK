import { Injectable, NgZone } from '@angular/core';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { GeoApi }  from '../sdk/services/index';
import { UserApi }    from '../sdk/services/index';
import { User}  from '../sdk/models/User';

import 'rxjs/add/operator/filter';
 
@Injectable()
export class LocationTracker {
  public user:User;
  public watch: any;    
  public lat: number =0;
  public lng: number =0;
  public time:number=0;
  constructor(public zone: NgZone,
              public backgroundGeolocation: BackgroundGeolocation,
              public geolocation: Geolocation,
              public geoApi:GeoApi,
              private userApi: UserApi
                 ) {
                 
 this.user=<User>this.userApi.getCachedCurrent();
   if (this.user===undefined) {
                             this.user.id=0;  
                                    }
  }
 
  putMyGeolocation(event_id:number,param: number) {
   
      // Background Tracking
  let self=this;

  let config = {
    desiredAccuracy: 0,
    stationaryRadius: 20,
    distanceFilter: 10, 
    debug: false,
    interval: 2000 
  };
 
  this.backgroundGeolocation.configure(config).first().subscribe((location) => {
 
    console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);
 
    // Run update inside of Angular's zone
    this.zone.run(() => {
      self.lat = location.latitude;
      self.lng = location.longitude;
      self.time=location.time;
      console.log('координаты бэкграунд'+location.latitude+' '+location.longitude+'  '+location.time );
     // location.time  self.geoApi.upsert();
      if (self.time===undefined) self.time=0;
      let model = {
       id:0,
       event_id:event_id,
       mechanic_id:this.user.id,
       lat:self.lat,
       lng:self.lng,
       date:self.time,
       device:'бэкграунд',
       id_when:param   //врямя принятия заявки
     }
      self.geoApi
               .create(model)
               .first()
               .subscribe(function(response: any) {
                console.log('координаты добавлены в базу успешно!');
                 self.stopTracking();
                 self.backgroundGeolocation.stop();
                },(error) => {console.log('Не удалось добавить координаты!');});
            });
 
  }, (err) => {
 
    console.log(err);
 
  });
 
  // Turn ON the background-geolocation system.
 this.backgroundGeolocation.start();
 
 
  // Foreground Tracking
 
let options = {
  frequency: 3000, 
  enableHighAccuracy: true
};
 
this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).first().subscribe((position: Geoposition) => {
 
  console.log('текущая позиция:'+position.coords.latitude+'  +  '+ position.coords.longitude);
 
  // Run update inside of Angular's zone
  this.zone.run(() => {
    self.lat = position.coords.latitude;
    self.lng = position.coords.longitude;
    self.time=position.timestamp;
     let model = {
       id:0,
       event_id:event_id,
       mechanic_id:this.user.id,
       lat:self.lat,
       lng:self.lng,
       date:self.time,
       device:'фореграунд',
       id_when:param   //врямя принятия заявки
     }
       console.log('координаты фореграунд '+position.coords.latitude+' '+position.coords.longitude );
      self.geoApi
               .create(model)
               .first()
               .subscribe(function(response: any) {
                console.log('координаты добавлены в базу успешно!');
                self.stopTracking();
                self.backgroundGeolocation.stop();
                },(error) => {console.log('Не удалось добавить координаты!');});
  });
 
});
 
 
  }
 
  stopTracking() {
   console.log('stopTracking');
 
    this.backgroundGeolocation.finish();
    this.watch.unsubscribe();
 
  }
   
}
