/* tslint:disable */
import {
  Liftelements
} from '../index';

declare var Object: any;
export interface ModuleElemInterface {
  "id": any;
  "element": any;
  liftelem?: Liftelements[];
}

export class ModuleElem implements ModuleElemInterface {
  "id": any;
  "element": any;
  liftelem: Liftelements[];
  constructor(data?: ModuleElemInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `ModuleElem`.
   */
  public static getModelName() {
    return "ModuleElem";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of ModuleElem for dynamic purposes.
  **/
  public static factory(data: ModuleElemInterface): ModuleElem{
    return new ModuleElem(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'ModuleElem',
      plural: 'ModuleElems',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "element": {
          name: 'element',
          type: 'any'
        },
      },
      relations: {
        liftelem: {
          name: 'liftelem',
          type: 'Liftelements[]',
          model: 'Liftelements'
        },
      }
    }
  }
}
