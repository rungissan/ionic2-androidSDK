/* tslint:disable */
import {
  Modules,
  ModuleElem
} from '../index';

declare var Object: any;
export interface LiftelementsInterface {
  "id": any;
  "element": any;
  "moduleid": any;
  "modelemid": any;
  modules?: Modules;
  modelem?: ModuleElem;
}

export class Liftelements implements LiftelementsInterface {
  "id": any;
  "element": any;
  "moduleid": any;
  "modelemid": any;
  modules: Modules;
  modelem: ModuleElem;
  constructor(data?: LiftelementsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Liftelements`.
   */
  public static getModelName() {
    return "Liftelements";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Liftelements for dynamic purposes.
  **/
  public static factory(data: LiftelementsInterface): Liftelements{
    return new Liftelements(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Liftelements',
      plural: 'Liftelements',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "element": {
          name: 'element',
          type: 'any'
        },
        "moduleid": {
          name: 'moduleid',
          type: 'any'
        },
        "modelemid": {
          name: 'modelemid',
          type: 'any'
        },
      },
      relations: {
        modules: {
          name: 'modules',
          type: 'Modules',
          model: 'Modules'
        },
        modelem: {
          name: 'modelem',
          type: 'ModuleElem',
          model: 'ModuleElem'
        },
      }
    }
  }
}
