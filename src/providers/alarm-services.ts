
import { Injectable} from '@angular/core';
import { EventsApi }  from '../sdk/services/index';
import { LiftApi }  from '../sdk/services/index';
import { UserApi }  from '../sdk/services/index';
import { ContractApi }  from '../sdk/services/index';
import { LiftelementsApi }  from '../sdk/services/index';
import { ModulesApi }  from '../sdk/services/index';
import { TroublesApi }  from '../sdk/services/index';
import { ToastController } from 'ionic-angular';
import * as moment from 'moment';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class AlarmsService {
  events : any [];
  lifts : any [];
  mechanics : any [];
  contracts : any [];
  modules : any [];
  liftelements : any [];
  problema: any [];
  base_time = new Date('1970-01-01T00:00:00.000Z');
  constructor(private contractApi: ContractApi,private eventsApi: EventsApi,
              private liftApi:LiftApi,private userApi:UserApi,private troublesApi:TroublesApi,
              private liftelementsApi: LiftelementsApi,private modulesApi: ModulesApi,private toastCtrl: ToastController) {
  console.log(this.base_time);};
  reformatData = function (element:any) {
                 // console.log("Привет от Мурки!"+ element['id']);
                 // console.log("Сам элемент"+ element);
                element['liftaddress'] = element.lift.address;
                element['userusername'] = element.user.username;
                element['contractaddress'] = element.contract.address; 
                element['date_receive']=moment(element.date).format('DD/MM/YYYY HH:mm:ss');
                element['buttoncolor']='green'; //green
                element['reporthide']=true; 
                element['listall']=element.id+' | '+element.date_receive;
               //  console.log('Данные  уже в пути!');
                var d= new Date(element.read_date);
                var d1 = new Date('1970-01-01T00:00:00.000Z');
                if (d.getTime()===d1.getTime()) {
                            element['date_get']='не принял';
                            element['buttoncolor']='red'; //red
                            } else {
                                    element['buttoncolor']='orange';  //orange
                                    element['date_get']=moment(element.read_date).format('DD/MM/YYYY HH:mm:ss');
                                   }
                var d_rep= new Date(element.report_date);
                if (d_rep.getTime()===d1.getTime()) {
                            element['date_report']='отчета нет';
                                               } else {
                                    element['buttoncolor']='green'; //green
                                    element['date_report']=moment(element.report_date).format('DD/MM/YYYY HH:mm:ss');
                                   }
                var d_up= new Date(element.report_date);
                if (d_up.getTime()===d1.getTime()) {
                            element['date_reportup']='не обновлялся';
                                                   } else {
                                                    element['date_reportup']=moment(element.report_date).format('DD/MM/YYYY HH:mm:ss');
                                   } 
                   return element;
           };
   getProblems(str:string,cb:any):any {
    var self = this;
     self.troublesApi
        .find( {
                where: { 'error_kod': {like: str} },
               })
        .subscribe(function(response: any) {
             cb(response);
         },
           (error) => {return str;}
         );
      return  str;
        }
  getModules(cb:any):any[] {
       this.modulesApi
         .find()
         .subscribe(function(response: any) {
           this.modules=response;
           cb(this.modules);
         });
      return  this.modules;
  }
   getLiftElements(cb:any):any[] {
       this.liftelementsApi
         .find()
         .subscribe(function(response: any) {
           this.liftelements=response;
           cb(this.liftelements);
         });
      return  this.liftelements;
  }
  getLifts(cb:any):any[] {
        this.liftApi
        .find()
        .subscribe(function(response: any) {
           this.lifts=response;
           cb(this.lifts);
         });
      return  this.lifts;
  }
   getMechanics(cb:any):any[] {
        this.userApi
        .find({
              where: {
                     or: [{'roleId': '2'},
                          {'roleId': '1'}
                         ]
                     }
              })
        .subscribe(function(response: any) {
           this.mechanics=response;
           cb(this.mechanics);
         });
      return  this.mechanics;
  }
    getContracts(cb:any):any[] {
        this.contractApi
        .find()
        .subscribe(function(response: any) {
           this.contracts=response;
           cb(this.contracts);
         });
      return  this.contracts;
  }
  getEvents(cb:any):any[] {
  let self=this;
     this.eventsApi
        .find({ include: ['lift','user','contract'],
                 order: 'id DESC'  }
             )        
        .subscribe(function(response: any) {
          // Process response
          self.presentToast('Данные  успешненько получены!');
           let doubles=response.map(self.reformatData);
           self.events=doubles;
           cb(response);
         },
          (error) => {self.presentToast('Не удалось добавить событие!');
                      cb( self.events ); });
       return  self.events;
  }
  updateEvents(item:string,model:any,cb:any):any[] {
      this.eventsApi
        .updateAll({ id: item}, model)
        .subscribe(function(response: any) {
          // Process response
          this.presentToast('Данные  успешно обновлены!');
          cb(this.events);
         });
        return  this.events;
   }
   addEvents(model: any,cb:any):any {
     this.eventsApi
       .create(model)
       .subscribe(function(response: any) {
       this.presentToast('Событие добавлено успешно!');
       cb(response);
    },
        (error) => {this.presentToast('Не удалось добавить событие!');});
         };
 
   presentToast(message:string) {
   let toast =this.toastCtrl.create({
    message: message,
    duration: 3000,
    position: 'top'
  });
  toast.present();
  }
  }
